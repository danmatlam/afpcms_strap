module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', '11.11.11.161'),
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'sandbox_escuela'),
        username: env('DATABASE_USERNAME', 'AdmAPPVtaGEN'),
        password: env('DATABASE_PASSWORD', 'V3nTas9834g3n'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
